import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductComponent } from './product/product.component';
import {InputModule} from "../../shared/input/input.module";
import {ButtonModule} from "../../shared/button/button.module";



@NgModule({
  declarations: [
    ProductsComponent,
    ProductComponent
  ],
  exports: [
    ProductsComponent,
  ],
  imports: [
    CommonModule,
    InputModule,
    ButtonModule,
  ]
})
export class ProductsModule { }
